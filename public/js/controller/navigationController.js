define([
	"jquery",
	"core/utility",
],
function($, utility) {

	'use strict';

	var NavigationView = {
		show: function($pageTo, $pageFrom) {
			// Hide/show pages
			$pageFrom.addClass('hide');
			$pageTo.removeClass('hide');
			
			// Show all the parents
			$pageTo.parents(".hide").removeClass('hide');

			// Scroll vertical position
			if(!$pageTo.attr('data-scroll-pos')){
			   $pageTo.attr('data-scroll-pos', window.pageYOffset);
            }
			if (window.scroll && window.scroll instanceof Function) {
				window.scroll(0, $pageTo.attr('data-scroll-pos') || 0);
			}
		}
	};

	function NavigationController() {
		var util = {
			/** Collapses the keyboard */
			dismissKeyboard: function() {
	            //$(':focus').trigger('blur');
	            //ie8 fix; ie8 may have focus on body and then blur will bring the window to background
				$(':focus').not('body').trigger('blur');

			},
			/** Performs page transitioning. */
			handlePageTransit: function(controller, page, $target, e) {
				e.preventDefault();

				this.dismissKeyboard();

	            // Extract page name
				page = page.substring(1, page.length);

				// Prevent navigation if no page is specified
				if (page.length === 0) {
					return;
				}

				// Determine the action to take (form submit, open popup, move to next page)
				if ($target.attr('data-form')) {
					this.handleFormSubmit(page, $('#' + $target.attr('data-form')), $target);
				} else if ($target.attr('type') === 'submit') {
					this.handleFormSubmit(page, $target.parents('form'), $target);
				} else {
					var $source = $(e.target);
					var $listItem = $source.parents('li');
					if ($listItem.length > 0) {
						$source = $listItem;
					}
					controller.switchToPage(page, $source);
				}
			},

			/** Performs form submission. */
			handleFormSubmit: function(page, $form, $target) {
				var data = {
					action: $form.attr('action'),
					formData: $form.serialize(),
					nextPage: page,
					$form: $form,
					$source: $target
				};
				// A neutal form submit trigger (Used by siteCatalyst)
				utility.trigger('form:submit', data);
				utility.trigger('form:submit:' + $form.attr('id'), data);
			}
		};

		return {
			init: function() {
				var self = this;
				// Bind listener for anchor and button clicks
				$('body').on('click', function(e) {
					var $target = $(e.target);
					// Buttons and anchors can have HTML tags inside of them
					var $parentButton = $target.parents('button');
					if ($parentButton.length > 0) {
						$target = $parentButton;
					}
					var $parentAnchor = $target.parents('a');
					if ($parentAnchor.length > 0) {
						
						if($parentAnchor.attr("data-web-link")) {
							utility.trigger("nav:weblink", $target);
							return;
						}
						$target = $parentAnchor;
					}
					//ignore the click on disabled obj
					if ($target.attr("disabled")) {
						return;
					}

					// Detect page - $target may be overwritten by handleSelector()
					var page = $target && ($target.attr('data-to-page')? $target.attr('data-to-page') :$target.attr('href'));
					if (page) {
						// if (page.indexOf('tel:') === 0) {
							// return;
						// }
						util.handlePageTransit(self, page, $target, e);
					}
				});

				utility.on('nav:switchToPage', function(page, data) {
					self.switchToPage(page, data);
				});
				utility.on('nav:submitPage', function(page) {
					var $form = $('#' + page).find('form');
					util.handleFormSubmit(page, $form, null);
				});
			},

			switchToPage: function(pageTo, $evt) {
				// Calculate pageFrom
				var $pageTo = pageTo instanceof $ ? pageTo : $("#" + pageTo),
				$pageFrom = $("#" + pageTo).siblings(":visible"),
				data = {
					fromPage: $pageFrom,
					toPage: $pageTo,
					evt : $evt
				};
				
				util.dismissKeyboard();
				
				utility.trigger("nav:leavePage", data);
				utility.trigger("nav:leavePage:" + $pageFrom.attr("id"), data);
				
				NavigationView.show($pageTo, $pageFrom);
				
				utility.trigger("nav:showPage", data);
				utility.trigger("nav:showPage:" + $pageTo.attr("id"), data);
			}
		};
	}

	return NavigationController();
});
