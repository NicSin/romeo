/*global define*/

define([
	"core/utility"
], 
function(utility) {

	'use strict';

	var AjaxController = function() {},
	
	ERRORS = {
		ERROR: "Error",
		BAD_DATA: "badDataError",
		NO_VIEW_NAME: "noViewNameError",
		NO_SUBSCRIBERS: "noSubscribersError"
	};

	AjaxController.prototype.init = function() {
		utility.on("ajax:error", this.onError, this);
		utility.on("ajax:response", this.onResponse, this);
	};

	AjaxController.prototype.onError = function(name, isErrorRetriable) {
		
	};	

	AjaxController.prototype.onResponse = function(data) {
		this.processResponse(data);
	};

	AjaxController.prototype.processResponse = function(data) {
		this.triggerEvents(data);
		utility.trigger("render:view:" + data.data.viewName, data);
	};	

	AjaxController.prototype.triggerEvents = function(data) {
		this.publicizeEvent(data);
	};	

	AjaxController.prototype.publicizeEvent = function(data) {
		utility.trigger("ajax:response:data", data);
	};	

	return new AjaxController();
});