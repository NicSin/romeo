'use strict';

require([
	"jquery",
	"controller/navigationController",
	"controller/ajaxController",
	"core/utility",
	
	"view/travellerView",
	"view/profileView",
	"view/activityView",
	"view/profileSummaryView"
],
function ($, navigationController, ajaxController, utility, TravellerView, ProfileView, ActivityView, ProfileSummaryView) {
	
    var app = {
        initialize: function () {
        	
        	// Initialize the navigation controller
        	navigationController.init();
        	
        	// Initialize Ajax controller
        	ajaxController.init();
        	
        	// Set context data
        	utility.setContext({
        		ajaxTimeout: 3000
        	});
        	
        	// Initialize views
            var traveller = new TravellerView();
            var profileSummaryView = new ProfileSummaryView();
            
            // setTimeout(function() {
            	// var profileView = new ProfileView();
//             	
            	// // setTimeout(function() {
	            	// // var activityView = new ActivityView();
	            // // }, 5000);
            // }, 5000);
        }
    };

    app.initialize();

});
