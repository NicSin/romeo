define([
	"jquery",
	"backbone"
],
function($, Backbone) {
	var TravellerModel = Backbone.Model.extend({
		
		defaults: {
		    mode: null,
		    from: null,
		    to: null,
		    departure: null,
		    name: null
		}
		
	});
	
	return TravellerModel;
});
