define([
	'jquery',
	"underscore",
	"core/baseView",
	"core/ajax",
	"core/utility"
],
function($, _, BaseView, ajax, utility) {
	
	'use strict';
	
	var ProfileSummaryView = BaseView.extend({
		
		template: 'profileSummaryView',
		
		init: function() {
			this.$hanger = $(".profileSummary");
			
			//Caching
			utility.viewRenderer.render(this.template, {});
			
			utility.on('traveller:hover', this.render, this);
			utility.on('traveller:overview', this.render, this);
		}
		
	});
	
	return ProfileSummaryView;
});
