define([
	'jquery',
	"underscore",
	"core/baseView",
	"core/ajax",
	"core/utility"
],
function($, _, BaseView, ajax, utility) {
	
	'use strict';
	
	var ProfileView = BaseView.extend({
		
		template: 'profileView',
		
		init: function() {
			utility.trigger('render:view:' + this.template, this.render, this);
		}
		
	});
	
	return ProfileView;
});
