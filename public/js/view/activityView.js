define([
	'jquery',
	"underscore",
	"core/baseView",
	"core/ajax",
	"core/utility"
],
function($, _, BaseView, ajax, utility) {
	
	'use strict';
	
	var ActivityView = BaseView.extend({
		
		template: 'activityView',
		
		init: function() {
			this.$hanger = $("#profileView .subView");
			utility.trigger('render:view:' + this.template, this.render, this);
		}
		
	});
	
	return ActivityView;
});
