define([
	"jquery",
	"underscore",
	"core/baseView",
	"core/ajax",
	"core/utility"
],
function($, _, BaseView, ajax, utility) {
	
	'use strict';
	
	var TravellerView = BaseView.extend({
		
		template: 'travellerView',
		
		el: $("#traveller"),
		
		events: {
			"mouseover .traveller"	: "profileHandler",
			"focusin   .traveller"	: "focusHandler",
			"focusout  .traveller"	: "focusHandler",
			"keyup     .traveller"	: "keyupHandler",
			"mouseout  #traveller"	: "mouseoutHandler"
		},
		
		init: function() {
			var $hanger = $(".smartTravellers");
			
			if($hanger.length > 0) {
				this.$hanger = $hanger;
			} else {
				this.$hanger = $( $("<div class='smartTravellers'></div>") );
				this.$el.append(this.$hanger);
			}
			
			_.bindAll(this, 'load');
			this.$hanger.on("scroll", this.load);
			
			this.load();
		},
		
		/**
		 * Overriding protected method to prevent $el removal
		 */
		_beforeRender: function(data) {
			
		},
		
		/**
		 * Overriding protected method to append content to traveller display
		 * @param {Object} content
		 * @param {Object} template
		 */
		_doRender: function(content, template, data) {
			var content = $(content);
			this.$hanger.append(content);
		},
		
		/**
		 * Overriding protected function to avoid postRender & nav:switchToPage event trigger
		 * @param {Object} content
		 * @param {Object} template
		 * @param {Object} data
		 */
		_afterRender: function(content, template, data) {
			
		},
		
		/**
		 * Overriding submit to add result clearing behaviour
		 * @param {Object} evt
		 */
		submit: function(evt) {
			this.$hanger.empty();
			BaseView.prototype.submit.call(this, evt);
		},
		
		load: function(evt) {
			var $target = evt && evt.target ? $(evt.target) : null, serialData = null;
			if($target) {
				
				var maxScrollTop = $target.prop('scrollHeight') - $target.height();
				var scrollAvailable = maxScrollTop - $target.scrollTop();
				
				if(scrollAvailable < 100) {
					
					serialData = this.serialize(this.$el.find('form'));
					this.sendRequest({
						url: "/fetch",
						method: "GET",
						ajaxTimeout: utility.getContext("ajaxTimeout"),
						data: {
							url: evt.action,
							data: evt.nextPage + '&' + serialData
						}
					});
				}
			} else {
				this.sendRequest({
					url: "/fetch",
					method: "GET",
					ajaxTimeout: utility.getContext("ajaxTimeout")
				});
			}
		},
		
		focusHandler: function(evt) {
			var $traveller = $(evt.target).closest('.traveller');
			evt.type === "focusin" ? $traveller.addClass("focused") : $traveller.removeClass("focused");
		},
		
		keyupHandler: function(evt) {
			var $traveller = $(evt.target).closest('.traveller');
			if(evt.type === "keyup") {
				if(evt.keyCode === 13) {
					$traveller.addClass("selected");
					this.profileHandler(evt);
				} else {
					 return;
				}
			}
		},
		
		profileHandler: function(evt) {
			var $traveller = $(evt.target).closest('.traveller'), data = $traveller.data();
			
			if( !_.isArray(data.rating) ) {
				var ratings = new Array();
				if(data.rating == 0) {
					ratings = ["", "", "", "", ""];
				} else {
					for(var i =0; i < 5; i++) {
						if(i < data.rating) {
							ratings.push("good");
						} else {
							ratings.push("bad");
						}
					}
				}
				data.rating = ratings;
			}
			
			utility.trigger('traveller:hover', {
				data: {
					profileSummaryView: data,
					evt : evt
				}
			});
		},
		
		mouseoutHandler: function(evt) {
			console.log(evt.target);
			utility.trigger('traveller:overview', {
				data: {
					profileSummaryView: {
						name: "What they say"
					}
				}
			});
		}
	});
	
	return TravellerView;
});
