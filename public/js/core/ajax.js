/*global define*/

define([
	"jquery",
	"core/utility"
], 
function ($, utility) {

	'use strict';

	var Ajax = function() {
	},
	
	ERRORS = {
		UNKNOWN 	: "unknown",
		TIMEOUT 	: "timeout",
		FORMAT  	: "format",
		PARSE_ERROR : "parsererror"
	};

	Ajax.prototype.send = function(config) {
		this.ajax(this.resolveMethod(config),
				  (config.headers || {}),
				  (config.url || ""),
				  this.contentType((config.data || "")),
				  this.stringify(config.data),
				  this.resolveSuccessCB(config),
				  this.resolveErrorCB(config),
				  config.ajaxTimeout);
	};

	Ajax.prototype.ajax = function(type, headers, url, contentType, data, successCB, errorCB, timeout) {
		$.ajax({
			type: type,
			headers: headers,
			url: url, 
			contentType:contentType,
			data: data,
			success: successCB,
			error: errorCB,
			timeout: timeout,
			dataType: "json"
		});
	};

	Ajax.prototype.onSuccess = function (data, textStatus, jqXHR) {
		utility.trigger("ajax:response", data);
	};

	Ajax.prototype.onError = function(jqXHR, textStatus, errorThrown) {
		var errorName = this.resolveErrorType(textStatus);
		utility.trigger("ajax:error", errorName, this.isErrorRetriable(errorName));
	};	

	Ajax.prototype.resolveMethod = function(config) {
		return (config.method || "POST");
	};		

	Ajax.prototype.resolveSuccessCB = function (config) {
		return ((config.success instanceof Function ) ? 
				config.success : 
				$.proxy(this.onSuccess, this));
	};

	Ajax.prototype.resolveErrorCB = function(config) {
		return ((config.error instanceof Function ) ? 
				config.error : 
				$.proxy(this.onError, this));
	};			

	Ajax.prototype.resolveErrorType = function(status) {
		var error = ERRORS.UNKNOWN;
		if(status === ERRORS.TIMEOUT) {
			error = ERRORS.TIMEOUT;
		} 
		else if(status === ERRORS.PARSE_ERROR) {
			// weird: mapping ParseError as Format Error
			error = ERRORS.FORMAT;
		}
		return error;
	};

	Ajax.prototype.isErrorRetriable = function(errorName) {
		return (errorName === ERRORS.TIMEOUT || errorName === ERRORS.FORMAT);
	};

	Ajax.prototype.stringify = function(data) {
		return (typeof data === "object" ? JSON.stringify(data) : data);
	};	

	Ajax.prototype.contentType = function(data) {
		return (typeof data === "object" ? "application/json; charset=utf-8" : "application/x-www-form-urlencoded; charset=UTF-8");
	};

	return new Ajax();
});