define([
	"jquery",
	"dust",
	"underscore",
	"backbone"
], function($, dust, _, Backbone) {
	/**
	 * An abstract view renderer implementation that"s based on Promises
	 * @constructor
	 */
	var ViewRenderer = function() {
		// Intentionally left blank
	};

	ViewRenderer.prototype = {

		/**
		 * The main public API for rendering a template
		 * @param template the name of the template to render
		 * @param context the context to pass to the renderer
		 * @returns a Promise
		 */
		render: function(template, context) {
			var deferred = new $.Deferred(), data = context;
			this._doRender(template, context, function(err, out) {
				if (err) {
					return deferred.reject(err, data);
				}
				deferred.resolve(out, template, data);
			});
			return deferred.promise();
		},

		/**
		 * The method to override to provide the view rendering implementation
		 * @private
		 * @param template the name of the template to render
		 * @param context the content to pass to the renderer
		 * @param callback the callback invoked when rendering is complete
		 */
		_doRender: function(template, context, callback) {
			// TODO: Implement
		}
	};



	/**
	 * A Dust view rendering implementation
	 * @constructor
	 */
	var DustRenderer = function(nougat) {
		var DEFAULT_PATH = "/templates/%s.js";

		dust.onLoad = function(name, callback) {

			var path = DEFAULT_PATH,
				template = path.replace("%s", name);
			require([template], function() {
				// Merely using requireJs to the load compiled template so undefining
				// it as soon as it's loaded so doesn't sit in the requireJs *and* dust.js
				// aches. Also, we know it's JS, thus doesn't need to be compiled so
				// callback has no arguments.
				require.undef(template);
				setTimeout(callback, 0);
			});
		};
	};

	DustRenderer.prototype = _.extend(ViewRenderer.prototype, {
		_doRender: function(template, context, callback) {
			var base = {};
			context = context || {};

			// Ugh.
			if (context.content) {
				base.cn = context.content;
				delete context.content;
			}

			context = dust.makeBase(base).push(context);
			dust.render(template, context, callback);
		}
	});
	
	var Util = function() {
		this.viewRenderer = new DustRenderer(this);
		this.context = new Backbone.Model();
	};
	
	Util.prototype.getContext = function(attribute) {
		return this.context.get(attribute);
	};
	
	Util.prototype.setContext = function(attributes) {
		this.context.set(attributes);
	};
	
	_.extend(Util.prototype, Backbone.Events);
	
	return new Util();
});
