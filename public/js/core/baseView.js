/**
 * Abstract view which enables rendering contents with a template.
 */
define([
	"underscore",
	"backbone",
	"core/utility",
	"core/ajax"
],
function(_, Backbone, utility, ajax) {

	'use strict';


	var BaseView = Backbone.View.extend({

		/**
		 * The name of the template that represents this view.
		 * Must be defined for render to succeed.
		 */
		template: null,
		
		/**
		 * Existing Dom element to append $el
		 * Must always be exisitng.
		 */
		$hanger: $('#content'),
		
		/**
		 * Default implementation of the initialize function.
		 */
		initialize: function() {
			var $el = $("#" + this.template);
			if($el.length > 0) {
				this.setElement($el, false);
			}
			
			utility.on('render:view:' + this.template, this.render, this);
			utility.on('form:submit:' + this.template + 'Form', this.submit, this);
			//this enables form fields in the event of a server error
			utility.on('response:error', function() {
	
				$("form:visible :input").removeAttr("disabled");
	
			},this);
			utility.on("nav:showPage:" + this.template, this._afterShow);
			
			this.init();
		},
		
		/**
		 * A handler to be invoked to initialize custom views
		 * @optional
		 */
		init: function() {
			// TODO: [optional] override
		},

		/**
		 * Protected' imlementation of what to do before template render result.
		 * A handler to be invoked prior to a template being rendered
		 */
		_beforeRender: function(data) {
			// TODO: [optional] override
			// Clean up DOM before rendering the template
			this.remove();
			this.preRender();
		},
		
		/**
		 * 'Protected' imlementation of what to do with template render result.
		 * Override to get access to raw content string.
		 * @param {String} content the rendered template string
		 * @param {String} template the name of this template
		 */
		_doRender: function(content, template, data) {
			var content = $(content);
			this.setElement(content, true);
			
			this.$hanger.append(this.$el);
		},
		
		/**
		 * Protected' imlementation of what to do after template render result.
		 * A handler to be invoked once template rendering is complete.
		 */
		_afterRender: function(content, template, data) {
			// TODO: [optional] override
			this.postRender();
			utility.trigger('nav:switchToPage', this.template, data.data.evt);
		},
		
		_afterShow: function(data) {
			//TODO Set focus on correct element here
			// if(data.evt.type === "keyup") {
				// data.toPage.find(":tabbable").focus();
			// }
		},
		
		/**
		 * A handler to be invoked prior to a template being rendered
		 */
		preRender: function(data) {
			// TODO: [optional] override
		},
		
		/**
		 * A default implementation of the standard Backbone render method.
		 * Handles rendering a template with the current view model.
		 * @returns the current view instance
		 */
		render: function(data) {
			var viewRenderer = utility.viewRenderer;

			_.bindAll(this, '_doRender', 'renderError', '_afterRender');

			this._beforeRender(data);

			viewRenderer.render(this.template, data)
				.done(this._doRender)
				.fail(this.renderError)
				.always(this._afterRender);

			return this;
		},

		/**
		 * A handler to be invoked once template rendering is complete.
		 */
		postRender: function(content, template, data) {
			// TODO: [optional] override
		},

		/**
		 * The error handler for template rendering
		 * @param {Error} err the error that occurred
		 */
		renderError: function(err, data) {
			// TODO: [optional] override
		},
		
		submit: function(evt) {
			// Allow control over default AJAX form submission
			evt.preventDefault = false;
			if (evt.preventDefault) {
				return;
			}
			
			// Validate & serialize form
			if (!this.validate(evt)) {
				return;
			}
	
			var serialData = this.serialize(evt.$form);
			if (evt.disableSubmit === true) {
				// Go to the next page
				this.navigate(evt.nextPage);
			} else {
				// Send AJAX request
				this.sendRequest({
					url: "/fetch",
					method: "GET",
					ajaxTimeout: utility.getContext("ajaxTimeout"),
					data: {
						url: evt.action,
						data: evt.nextPage + '&' + serialData
					}
				});
			}
		},
		
		/**
		 * A handler to validate form.
		 */
		validate: function(evt) {
			// TODO: [optional] override
			return true;
		},
		
		/**
		 * A handler to validate form.
		 */
		sendRequest: function(config) {
			// Send AJAX request
			utility.trigger("formView:sendRequest",config);
			ajax.send(config);
		},

		/**
		 * Gets the current model or collection in JSON form.
		 * @returns
		 */
		serialize: function($form) {
			return $form.serialize();
		}
	});

	return BaseView;
});
