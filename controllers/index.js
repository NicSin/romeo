'use strict';


module.exports = function (server) {

    server.get('/', function (req, res) {
        var model = { name: 'romeo' };
        
        res.render('indexView', model);
        
    });
    server.get('/profile', function (req, res) {
        var model = { name: 'romeo' };
        
        res.render('profileView', model);
        
    });
    
    server.get('/fetch', function (req, res) {
        var model = {
        	data: {
        		viewName: "travellerView",
        		travellerView : [
		        	{
		        		imgsrc: "img/nicsin.jpg",
					    mode: "flight",
					    from: "Delhi, India",
					    to: "San Fransisco, USA",
					    departure: "1 May 2014",
					    name: "Nitin Singh",
					    rating: 3
					}, {
		        		imgsrc: "img/nicsin.jpg",
					    mode: "flight",
					    from: "Delhi, India",
					    to: "San Fransisco, USA",
					    departure: "1 May 2014",
					    name: "Nitin Singh",
					    rating: 3
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}, {
					    mode: "train",
					    from: "San Fransisco, USA",
					    to: "Los Angales, USA",
					    departure: "31st Dec, 2013",
					    name: "David Letterman"
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}, {
					    mode: "train",
					    from: "San Fransisco, USA",
					    to: "Los Angales, USA",
					    departure: "31st Dec, 2013",
					    name: "David Letterman"
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}, {
					    mode: "train",
					    from: "San Fransisco, USA",
					    to: "Los Angales, USA",
					    departure: "31st Dec, 2013",
					    name: "David Letterman"
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}, {
					    mode: "train",
					    from: "San Fransisco, USA",
					    to: "Los Angales, USA",
					    departure: "31st Dec, 2013",
					    name: "David Letterman"
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}, {
					    mode: "train",
					    from: "San Fransisco, USA",
					    to: "Los Angales, USA",
					    departure: "31st Dec, 2013",
					    name: "David Letterman"
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}, {
					    mode: "train",
					    from: "San Fransisco, USA",
					    to: "Los Angales, USA",
					    departure: "31st Dec, 2013",
					    name: "David Letterman"
					}, {
					    mode: "cycle",
					    from: "San Fransisco",
					    to: "San Jose",
					    departure: "23 Nov, 2013",
					    name: "Rakesh Koul"
					}
       			]
        	}
        };
        
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(JSON.stringify(model));
        
    });

};
